﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisibleDrunkyard : MonoBehaviour
{
    enum Tiles
    {
        empty,
        floor,
        wall
    }

    Tiles[,] grid;



    struct Walker // first time using this, but seems really usefull
    {
        public Vector2 _wdirection;
        public Vector2 _wposition;
    }
    List<Walker> walkers;

    [SerializeField]
    private GameObject _floortile;
    [SerializeField]
    private GameObject _walltile;
    [SerializeField]
    private GameObject _walkertile;

    public int _levelheight;
    public int _levelwidht;

    public int _walkernumber = 1;
 

    public float fillpercent = 0.5f;
    public float movmentrate = 0.5f;



    // Start is called before the first frame update
    void Start()
    {
        Setup();
        MoveWalker();       
        fillLevel();
        InvokeRepeating("MoveWalker", movmentrate, movmentrate);
    }


   

    void Setup()
    {
        //generate grid and sets it to empty
        grid = new Tiles[_levelwidht, _levelheight];
        for (int x = 0; x < _levelwidht; x++)
        {
            for (int y = 0; y < _levelheight; y++)
            {
                grid[x, y] = Tiles.wall; //sets internal tiles to empty at start
            }
        }

        walkers = new List<Walker>();
        CreateWalker(_walkernumber);
    }

    void CreateWalker(int number)
    {
        for (int i = 0; i < number; i++)
        {
            Walker nwalker = new Walker();
            nwalker._wdirection = changeDirection();
            Vector2 start = new Vector2(Mathf.RoundToInt(_levelwidht / 2.0f), Mathf.RoundToInt(_levelheight / 2.0f));
            nwalker._wposition = start;
            walkers.Add(nwalker);
        }

    }

    void MoveWalker()
    {
        if ((float)ptilenumber() / (float)grid.Length > fillpercent)
        {
            return;
        }
        foreach (Walker walker in walkers)
        {
            if (grid[(int)walker._wposition.x, (int)walker._wposition.y] != Tiles.floor)
            {
                grid[(int)walker._wposition.x, (int)walker._wposition.y] = Tiles.floor;

                Spawn(_floortile, walker._wposition.x, walker._wposition.y);
                //VisibleSpawn(_walkertile, walker._wposition.x, walker._wposition.y);
            }
            VisibleSpawn(_walkertile, walker._wposition.x, walker._wposition.y);


        }

        for (int i = 0; i < walkers.Count; i++)
        {
            Walker thisWalker = walkers[i];
            thisWalker._wdirection = changeDirection();
            walkers[i] = thisWalker;
        }

        for (int i = 0; i < walkers.Count; i++)
        {
            Walker walker = walkers[i];
            walker._wposition += walker._wdirection;
            walkers[i] = walker;
        }

        for (int i = 0; i < walkers.Count; i++)
        {
            Walker walker = walkers[i];
            walker._wposition.x = Mathf.Clamp(walker._wposition.x, 1, _levelwidht - 2);
            walker._wposition.y = Mathf.Clamp(walker._wposition.y, 1, _levelwidht - 2);
            walkers[i] = walker;
        }

        
    }
    int ptilenumber()
    {

        int count = 0;
        foreach (Tiles space in grid)
        {
            if (space == Tiles.floor)
            {
                count++;
            }
        }
        return count;
    }



    Vector2 changeDirection()
    {
        int number = Mathf.FloorToInt(Random.value * 3.99f); // Random.value gets a number between 0 and 1, floor to int means, that including 0 to 3 (max is 3.99->3)we have 4 possible cases 

        switch (number)
        {
            case 0:
                return Vector2.down;
            case 1:
                return Vector2.left;
            case 2:
                return Vector2.up;
            default:
                return Vector2.right;
        }
    }

    void fillLevel()
    {
        for (int x = 0; x < _levelwidht; x++)
        {
            for (int y = 0; y < _levelheight; y++)
            {
                switch (grid[x, y])
                {
                    case Tiles.empty:
                        break;
                    case Tiles.floor:
                        Spawn(_floortile, x, y);
                        break;
                    case Tiles.wall:
                        Spawn(_walltile, x, y);
                        break;

                }
            }
        }
    }
   

    

    void Spawn(GameObject spawed, float x, float y)
    {
        GameObject @object = Instantiate(spawed, new Vector3(x, y, 0), Quaternion.identity);
        @object.transform.parent = gameObject.transform; // used to keep the inspector clean, could be used to maybe keep as prefabs?
    }
    void VisibleSpawn(GameObject spawed, float x, float y)
    {
        GameObject wobject = Instantiate(spawed, new Vector3(x, y, 0), Quaternion.identity);
        wobject.transform.parent = gameObject.transform; // used to keep the inspector clean, could be used to maybe keep as prefabs
        Destroy(wobject, movmentrate);
    }



    
}
